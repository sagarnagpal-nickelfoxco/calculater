//
//  ViewController.swift
//  Calculater
//
//  Created by Nickelfox on 07/08/21.
//




import UIKit
import Foundation

protocol HomeViewControllerDelegate: class {
    var view: HomeViewModelDelegate? { get set }
    func digitPressed(digit: String) -> String
    func operandPressed(operand: String) -> String
}

class HomeViewController:UIViewController, UITextFieldDelegate {
    @IBOutlet weak var digitsTextfield: UITextField!
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet weak var decimal: UIButton!
    var viewModel: HomeViewControllerDelegate!
    var isFirstDigit = true
    var operand1: Double = 0
    var operation = "="
    var displayValue: Int {
        get {
            return Int(Double(NumberFormatter().number(from: digitsTextfield.text!)!.intValue))
        }
        set {
            digitsTextfield.text = "\(newValue)"
            isFirstDigit = true
            operation = "="
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        digitsTextfield.delegate = self
        self.initialSetup()
    }
    
    
    private func initialSetup() {
        self.setupUI()
        self.setupViewModel()
    }
    
    
    
    private func setupUI() {
        decimal.backgroundColor = Colors.grey
        decimal.titleLabel?.font =  UIFont.systemFont(ofSize: 30)
        decimal.layer.cornerRadius = decimal.frame.height/2
        for b in buttons{
            b.backgroundColor =  Colors.lightGrey
            b.layer.cornerRadius = b.frame.height/2
            switch b.tag{
            case 16..<19:
                b.backgroundColor =  Colors.lightGrey
                b.titleLabel?.font =  UIFont.systemFont(ofSize: 30)
                break
            case 0..<11:
                b.backgroundColor =  Colors.grey
                b.titleLabel?.font =  UIFont.systemFont(ofSize: 30)
                break
            case 10:
                b.backgroundColor =  Colors.grey
                b.titleLabel?.font =  UIFont.systemFont(ofSize: 30)
                
                break
            case 11..<16:
                b.backgroundColor = Colors.yellow
                b.titleLabel?.font =  UIFont.systemFont(ofSize: 40)
                break
            default:
                print("")
            }
        }
    }
    
    private func setupViewModel() {
        if self.viewModel == nil {
            self.viewModel = HomeViewModel(self)
        }
    }
    
    // digit Pressed from 1 t0 9 and . included.
    @IBAction func touchDigit(_ sender: UIButton) {
        
        let digit = self.viewModel.digitPressed(digit: sender.currentTitle!)
        digitsTextfield.text = isFirstDigit ? digit : digitsTextfield.text! + digit
        isFirstDigit = false
        if (digit == "."){
            self.decimal.isEnabled = false
        }
    }
    
    
    // clear screen
    @IBAction func clearDisplay(sender: AnyObject) {
        displayValue = 0
        self.decimal.isEnabled = true
    }
    
    // any operenad is pressed like + - / *
    @IBAction func saveOperand(sender: UIButton) {
        operation =  self.viewModel.operandPressed(operand: sender.currentTitle!)
        operand1 = Double(displayValue)
        isFirstDigit = true
        self.decimal.isEnabled = true
    }
    
    // +/- is pressed
    @IBAction func negate(_ sender: UIButton) {
        displayValue = 0 - displayValue
    }
    
    
    // % is pressed
    @IBAction func perc(_ sender: UIButton) {
        displayValue /= 100
    }
    
    //calculation perfform on = pressed
    @IBAction func calculate(_ sender: UIButton) {
        switch operation {
        case "/":
            if ((displayValue == 0) || (displayValue == 00)) {
                self.decimal.isEnabled = true
                isFirstDigit = true
            }
            else {
                displayValue = Int(operand1) / displayValue
                self.decimal.isEnabled = true
            }
            break
        case "*":displayValue *= Int(operand1)
            self.decimal.isEnabled = true
            break
        case "+":displayValue += Int(operand1)
            self.decimal.isEnabled = true
            break
        case "-":displayValue = Int(operand1) - displayValue
            self.decimal.isEnabled = true
            break
        default:break
        }
    }
}
extension HomeViewController: HomeViewModelDelegate {
}
