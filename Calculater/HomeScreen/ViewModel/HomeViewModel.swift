//
//  HomeViewModel.swift
//  Calculater
//
//  Created by Nickelfox on 07/08/21.
//

import Foundation
import UIKit


protocol HomeViewModelDelegate: BaseViewModelProtocol {
}

class HomeViewModel {
    var view: HomeViewModelDelegate?
    init(_ view: HomeViewModelDelegate) {
        self.view = view
    }
}

extension HomeViewModel: HomeViewControllerDelegate {
    func operandPressed(operand: String) -> String {
        switch operand {
        case "÷":return "/"
        case "x":return "*"
        case "+":return "+"
        case "-":return "-"
        default:
            return "0"
        }
    }
    
    func digitPressed(digit: String) -> String {
        switch digit {
        case "1":return "1"
        case "2":return "2"
        case "3":return "3"
        case "4":return "4"
        case "5":return "5"
        case "6":return "6"
        case "7":return "7"
        case "8":return "8"
        case "9":return "9"
        case ".":return "."
        default:
            return "0"
        }
    }
}
    

