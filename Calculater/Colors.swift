//
//  Colors.swift
//  aris
//
//  Created by Ravindra Soni on 11/12/18.
//  Copyright © 2018 Nickelfox. All rights reserved.
//

import Foundation
import FLUtilities

struct Colors {
    
	static var yellow : UIColor {
		return UIColor(hexString: "ff9e0c")
	}
    
    
    static var lightGrey: UIColor {
        return UIColor(hexString: "a5a5a5")
    }
    
    static var grey: UIColor {
        return UIColor(hexString: "333333")
    }
    

}
