//
//  BaseViewModel.swift
//  aris
//
//  Created by Vaibhav Parmar on 12/05/21.
//  Copyright © 2021 Nickelfox. All rights reserved.
//

import Foundation
import UIKit
protocol BaseViewModelProtocol: class {
    func showError(message: String)
    func showAlert(message: String)
    func showNoInternetAlert()
    func showDefaultAlert(message: String)
    func showSnackBar(_ message: String)
    func showDialogAlert(message: String)
}

extension UIViewController: BaseViewModelProtocol {
    
    func showError(message: String) {
    }
    
    func showAlert(message: String) {
    }
    
    func showNoInternetAlert() {
    }
    
    func showDefaultAlert(message: String) {
    }
    
    func showSnackBar(_ message: String) {
    }
    
    func showDialogAlert(message: String) {
    }

}
